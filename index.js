console.log('Hello, World')
// Details
const details = {
    fName: "Joseph",
    lName: "Soliva",
    age: 18,
    hobbies : [
        "Playing", "Gaming", "Listening to music"
    ] ,
    workAddress: {
        housenumber: "Lot 3, Block B",
        street: " Emerald Avenue, Ortigas Center ",
        city: "NCR",
        state: "Pasig City",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");
